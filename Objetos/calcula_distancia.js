//p1 y p2 son objetos en javascript con sus valores x , y
//y con sus funciones moverEnX y moverEnY
const p1 = {
  x: 0, //Asignacion de valores
  y: 4,
  moverEnX: function (x) { this.x += x },//Metodo del objeto, se puede hacer en varias lineas {codigo}
  moverEnY: function (y) {this.y += y}//this = referencia al parametro que se tiene
}
/**Llamar a la funcion del objeto
p1.moverEnX(5)
p2.moverEnY(10)
*/

const p2 = {
  x: 3,
  y: 0,
  moverEnX: function (x) { this.x += x },
  moverEnY: function (y) {this.y += y}
}

function distancia(p1,p2) {
  const x= p1.x - p2.x
  const y = p1.y - p2.y
  //Math.sqrt() = para sacar la raiz cuadrada
  return Math.sqrt(x * x + y * y)
}
console.log(distancia(p1, p2));
//console.log(distancia(p1, { x:20, y: -7 }));
