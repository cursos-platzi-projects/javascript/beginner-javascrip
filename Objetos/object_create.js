//Modificando la clase de prototipos.js
//Esta es la segunda parte de como crear objetos muy similar a la primera

//const Punto = {} : Es un objeto no una funcion como lo anterior
const Punto = {
  //init es un ejemplo pero se le puede poner crear, inicio, etc
  init: function init(x, y) {
    this.x = x
    this.y = y
  },
  moverEnX: function moverEnX(x) {
    this.x += x
  },
  moverEnY: function moverEnY(y) {
    this.y += y
  },
  distancia: function distancia(p) {
    const x = this.x - p.x
    const y = this.y - p.y

    return Math.sqrt(x * x + y * y)
  }
}
//Object.create() = es como decir un new, creando un objeto
const p1 = Object.create(Punto)
const p2 = Object.create(Punto)
//Se tiene que poner esto, ya que javascript no reconoce que es el init
p1.init(0, 4)
p2.init(3, 0)

console.log(p1.distancia(p2));
console.log(p2.distancia(p1));
p1.moverEnX(10)
console.log(p1.distancia(p2));
p2.moverEnY(-4)
console.log(p1.distancia(p2));
