//Esto es para mejorar el codigo de calcula_distancia.js
//ya que se repiten los metodos moverEnX y moverEnY en ambos objetos "p1 y p2"
//Nota: Los objetos estan basados en prototipos

function Punto(x, y) {
  this.x = x
  this.y = y
}
//prototype = hace que se le asigne un metodo al Punto para cuando lo instancias el objeto tenga ese metodo
//Esto es para que los metodos se creen de manera generica y todos los que instanceen de Punto lo puedan tener
//Para eso es mejorar el codigo y no se repita mas
//A Punto se le asignan funciones como moverEnX, moverEnY, distancia
Punto.prototype.moverEnX = function moverEnX(x) {
  this.x += x
}

Punto.prototype.moverEnY = function moverEnY(y) {
  this.y += y
}

Punto.prototype.distancia = function distancia(p) {
  const x = this.x - p.x
  const y = this.y - p.y

  //Math.sqrt = para sacar la raiz cuadrada
  return Math.sqrt(x * x + y * y)
}

const p1 = new Punto(0, 4)

const p2 = new Punto(3, 0)


console.log(p1.distancia(p2));
console.log(p2.distancia(p1));
p1.moverEnX(10)
console.log(p1.distancia(p2));
p2.moverEnY(-4)
console.log(p1.distancia(p2));
//console.log(distancia(p1, { x:20, y: -7 }));
