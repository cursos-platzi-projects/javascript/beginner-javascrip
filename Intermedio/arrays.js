/*El método map() crea un nuevo array con los resultados de la llamada
a la función indicada aplicados a cada uno de sus elementos.*/
/*El método filter() crea un nuevo array con todos los 
elementos que cumplan la condición implementada por la función dada.
*/

//Expresed operator, para poner en los parametro una variable dinamica = (...variable)
function suma(...numeros) {
  /*let acum = 0;
  for (let i = 0; i < numeros.length; i++){
    acum += numeros[i]
  }*/
  //Todos los arrays tienen este metodo 'reduce'
  return numeros.reduce(function (acum, numero){
    acum += numero
    return acum
  }, 0)
}

/*
Pero esta funcion se puede reducir mejor
function dobles(...numeros) {
  const resultado = []
  for (let i = 0; i < numeros.length; i++) {
    resultado.push(numeros[i] * 2)
  }
  return resultado
}
*/

/*function pares(...numeros) {
  const resultado = []
  const length = numeros.length
  for (let i = 0; i < length; i++) {
    const numero = numeros[i]
    if (numero % 2 == 0) {
      resultado.push(numero)
    }
  }
  return resultado
}*/
//Manera correcta sustituyendo la funcion de arriva
const dobles = (...numeros) => numeros.map(numero => numero * 2)
const pares = (...numeros) => numeros.filter(numero => numero % 2 == 0)

suma(4, 8, 12, 8954, 7)
