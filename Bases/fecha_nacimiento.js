//Creando nuevo objeto de tipo Date
const nacimiento = new Date(1991, 9, 25)
const hoy = new Date();

const tiempo = hoy - nacimiento

const tiempoSegundos = tiempo / 1000

const tiempoMin = tiempoSegundos / 60

const tiempoHoras = tiempoMin / 60

//Que dia cae mi cumple este año (lunes,martes,miercoles)
const proximo = new Date(hoy.getFullYear(), nacimiento.getMonth(), nacimiento.getDate())

//Para que sea en español
const diasSemana = [
  "Domingo",
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes",
  "Sábado"
]

diasSemana[proximo.getDay()]
