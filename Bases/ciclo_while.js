let vidaGoku = 100
let vidaSuperman = 100

const MIN_POWER = 5
const MAX_POWER = 12

const ambosSiguenVivos = () => vidaGoku > 0 && vidaSuperman > 0

const calcularGolpe = () => Math.random() * (MAX_POWER - MIN_POWER) + MIN_POWER

const gokuSiguiVivo = () => vidaGoku > 0

let round = 0

while (ambosSiguenVivos()) {
  round++
  console.log(`Round ${round}`)
  //calcularGolpe = Function creado para saber cual de los dos tiene mayor valor y asi pueda atacar
  const golpeGoku = calcularGolpe()
  const golpeSuperman = calcularGolpe()

  if (golpeGoku > golpeSuperman) {
    //Ataca goku
    console.log(`Goku ataca a superman con un golpe de ${golpeGoku}`)
    vidaSuperman -= golpeGoku
    console.log(`Superman queda en ${vidaSuperman} de vida`);
  }
  else {
    //Ataca superman
    console.log(`Superman ataca a Goku con un golpe de ${golpeSuperman}`)
    vidaGoku -= golpeSuperman
    console.log(`Goku queda en ${vidaGoku} de vida`)
  }
}

if (gokuSiguiVivo()){
  console.log(`Goku gano la pelea. Su vida es de ${vidaGoku}`)
}
else {
  console.log(`Superman gano la pelea. Su vida es de ${vidaSuperman}`)
}
