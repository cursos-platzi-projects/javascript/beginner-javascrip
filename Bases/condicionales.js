// los valores de una constante(const) no se pueden modificar
const startWars7 = 'Start Wars: El despertar de la fuerza'
const pgStartWars7 = 13

const nameGianni = 'Gianni'
const ageGianni = 25

const nameJose = 'Jose'
const ageJose = 12
//isWithAdult = false : Valor por defecto en una funcion
function canWatchStarWars7(name,age, isWithAdult = false){
  if (age >= pgStartWars7) {
    alert(`${name} puede pasar a ver ${startWars7}`)
  } else if (isWithAdult) {
    alert(`${name} puede pasar a ver ${startWars7}.
      Aunque su edad es ${age}, se encuentra acompañada/o por un adulto`)
  }else {
    alert(`${name} no puede pasar a ver ${startWars7}.
      Tiene ${age} años y necesita tener ${pgStartWars7}`)
  }
}
canWatchStarWars7(nameGianni,ageGianni)
canWatchStarWars7(nameJose,ageJose, true)
