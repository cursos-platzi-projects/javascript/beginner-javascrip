//NOTA: Por mas  que en javascript le llames class a una clase aun asi sigue siendo un prototipo.
class Persona {
    constructor (nombre, apellido, altura) {
        this.nombre = nombre
        this.apellido = apellido
        this.edad = 20
        this.altura = altura
    }

    saludar() {
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
    }

    soyAlto() {
        return this.altura > 1.8
    }
}

class Desarrollador extends Persona {
    constructor (nombre, apellido, altura) {
        //Para llamar al constructor de la clase padre
        super(nombre, apellido, altura)
        //Despues del super ya se puede usar el 'this'

        //Y asi ya no llamar a estas propiedades
        /*this.nombre = nombre;
        this.apellido = apellido;
        this.altura = altura;*/
    }

    saludar() { 
        console.log(`Hola, soy ${this.nombre} ${this.apellido} y soy desarrollador/a`)
    }
}